module github.com/hedzr/go-ringbuf/v2

go 1.18

// replace github.com/hedzr/rules v0.0.0 => ../rules

// replace github.com/hedzr/pools v0.0.0 => ../pools

// replace github.com/hedzr/errors v0.0.0 => ../errors

require (
	github.com/hedzr/log v1.5.47
	golang.org/x/sys v0.0.0-20220406163625-3f8b81556e12
	gopkg.in/hedzr/errors.v3 v3.0.17
)
